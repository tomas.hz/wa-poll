from __future__ import annotations

import datetime
import hashlib
import http.client
import io
import typing
import uuid

import flask
import flask_babel
import flask_wtf
import matplotlib.pyplot
import sqlalchemy
import werkzeug.exceptions
import wtforms
import wtforms.validators

from .. import database

__all__ = [
	"PollChoiceForm",
	"list_",
	"poll_blueprint",
	"view",
	"view_image"
]


poll_blueprint = flask.Blueprint(
	"poll",
	__name__,
	url_prefix="/polls"
)


class PollChoiceForm(flask_wtf.FlaskForm):
	choice = wtforms.fields.RadioField(
		flask_babel.lazy_gettext("Choice")
	)


@poll_blueprint.route("/")
def list_() -> typing.Tuple[flask.Response, int]:
	polls = flask.g.sa_session.execute(
		sqlalchemy.select(database.Poll)
	).scalars().all()

	return flask.render_template(
		"poll_list.html",
		polls=polls
	), http.client.OK


@poll_blueprint.route("/<uuid:id_>", methods=["GET", "POST"])
def view(id_: uuid.UUID)  -> typing.Tuple[flask.Response, int]:
	# Delete past identifiers for the sake of privacy
	flask.g.sa_session.execute(
		sqlalchemy.update(database.Response).
		where(
			database.Response.created_on <= (
				datetime.datetime.utcnow()
				- datetime.timedelta(
					seconds=flask.current_app.config["RESPONSE_IDENTIFIER_MAX_AGE"]
				)
			)
		).
		values(
			respondent_identifier=None
		)
	)

	flask.g.sa_session.commit()

	poll = flask.g.sa_session.execute(
		sqlalchemy.select(database.Poll).
		where(database.Poll.id == id_)
	).scalars().one_or_none()

	if poll is None:
		raise werkzeug.exceptions.NotFound

	choices = flask.g.sa_session.execute(
		sqlalchemy.select(database.Choice).
		where(
			database.Choice.poll_id == poll.id
		)
	).scalars().all()

	form = PollChoiceForm()

	form.choice.choices = [
		choice.name
		for choice in choices
	]

	if form.validate_on_submit():
		for choice in choices:
			if choice.name == form.choice.data:
				chosen = choice

		identifier = hashlib.scrypt(
			flask.request.remote_addr.encode("utf-8"),
			salt=flask.current_app.config["SCRYPT_PEPPER"].encode("utf-8"),
			n=16384,
			r=8,
			p=1,
			maxmem=0,
			dklen=32
		).hex()

		voted_before = flask.g.sa_session.execute(
			sqlalchemy.select(database.Response.id).
			where(
				database.Response.respondent_identifier == identifier
			).
			exists().
			select()
		).scalars().one()

		if voted_before:
			flask.abort(http.client.FORBIDDEN)

		flask.g.sa_session.add(
			database.Response(
				choice_id=chosen.id,
				respondent_identifier=identifier
			)
		)

		chosen.response_count += 1

		flask.g.sa_session.commit()

	return flask.render_template(
		"poll_view.html",
		poll=poll,
		choices=choices,
		form=form
	)


@poll_blueprint.route("/<uuid:id_>/image")
def view_image(id_: uuid.UUID)  -> typing.Tuple[flask.Response, int]:
	poll = flask.g.sa_session.execute(
		sqlalchemy.select(database.Poll).
		where(database.Poll.id == id_)
	).scalars().one_or_none()

	if poll is None:
		raise werkzeug.exceptions.NotFound

	choices = flask.g.sa_session.execute(
		sqlalchemy.select(database.Choice).
		where(
			database.Choice.poll_id == poll.id
		)
	).scalars().all()

	names = []
	votes = []

	for choice in choices:
		names.append(choice.name)
		votes.append(choice.response_count)

	figure = matplotlib.pyplot.figure(figsize=(10, 5))

	matplotlib.pyplot.bar(
		names,
		votes,
		color="maroon",
		width=0.4
	)

	matplotlib.pyplot.xlabel(
		flask_babel.gettext("Response")
	)

	matplotlib.pyplot.ylabel(
		flask_babel.gettext("Vote count")
	)

	matplotlib.pyplot.title(
		flask_babel.gettext("Respondents and their votes for: ")
		+ poll.name
	)

	image_buffer = io.BytesIO()
	figure.savefig(image_buffer, format="png")
	image_buffer.seek(0)

	return flask.send_file(
		image_buffer,
		mimetype="image/png",
		as_attachment=False,
		download_name=(
			flask_babel.gettext("votes")
			+ ".png"
		)
	), http.client.OK
