import datetime

import sqlalchemy

from . import Base
from .utils import UUID, get_uuid

__all__ = ["Choice", "Poll", "Response"]


class Poll(Base):
	__tablename__ = "polls"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	name = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)

	description = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=False
	)


class Choice(Base):
	__tablename__ = "choices"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	poll_id = sqlalchemy.Column(
		UUID,
		sqlalchemy.ForeignKey(
			"polls.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	)

	name = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)
	""".. warning::
		Names should be unique within a single poll.
	"""

	response_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		nullable=False,
		default=0
	)


class Response(Base):
	__tablename__ = "responses"

	id = sqlalchemy.Column(
		UUID,
		primary_key=True,
		default=get_uuid
	)

	respondent_identifier = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=False
	)

	created_on = sqlalchemy.Column(
		sqlalchemy.DateTime,
		nullable=False,
		default=datetime.datetime.utcnow
	)

	choice_id = sqlalchemy.Column(
		UUID,
		sqlalchemy.ForeignKey(
			"choices.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	) 
